import 'package:flutter/material.dart';

class FlutterIsLoveLayoutBuilder extends StatelessWidget {
  final double size;

  const FlutterIsLoveLayoutBuilder({Key? key, required this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return LayoutBuilder(builder: (context, constraints) {
      final wideEnough = constraints.maxWidth > size*2+50 && constraints.maxWidth>constraints.maxHeight;
      return wideEnough
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FlutterLogo(size: size),
                  Icon(Icons.favorite, color: Colors.red, size: size),
                ],
              ) :
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FlutterLogo(size: size),
                  Icon(Icons.favorite, color: Colors.red, size: size),
                ],
              );
    });

    // return screenWidth>screenHeight? Row(
    //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //     children: [
    //       FlutterLogo(size: size),
    //       Icon(Icons.favorite, color: Colors.red, size: size),
    //     ],
    // ) : Column(
    //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //   children: [
    //     FlutterLogo(size: size),
    //     Icon(Icons.favorite, color: Colors.red, size: size),
    //   ],
    // );
  }
}