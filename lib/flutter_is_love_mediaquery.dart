import 'package:flutter/material.dart';

class FlutterIsLoveMediaQuery extends StatelessWidget {
  final double size;

  const FlutterIsLoveMediaQuery({Key? key, required this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    return screenWidth>screenHeight && screenWidth>size*2+50? Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          FlutterLogo(size: size),
          Icon(Icons.favorite, color: Colors.red, size: size),
        ],
    ) : Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        FlutterLogo(size: size),
        Icon(Icons.favorite, color: Colors.red, size: size),
      ],
    );
  }
}